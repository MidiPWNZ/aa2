package Person;

import Identity.Identity;

/**
 * Representation of a person, with an identity.
 * 
 * @author aschulz
 *
 */
public class Person extends Identity
{
	private String name;
	private String vorname;
	private String mail;
	private String fon;
	private String street;
	private String number;
	private String zip_code;
	private String place;

	@SuppressWarnings("unused")
	private Person()
	{}

	public Person(String name, String vorname, String mail, String fon, String street, String number, String zip_code,
			String place)
	{
		this.name = name;
		this.vorname = vorname;
		this.mail = mail;
		this.fon = fon;
		this.street = street;
		this.number = number;
		this.zip_code = zip_code;
		this.place = place;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getVorname()
	{
		return vorname;
	}

	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}

	public String getMail()
	{
		return mail;
	}

	public void setMail(String mail)
	{
		this.mail = mail;
	}

	public String getFon()
	{
		return fon;
	}

	public void setFon(String fon)
	{
		this.fon = fon;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

	public String getZip_code()
	{
		return zip_code;
	}

	public void setZip_code(String zip_code)
	{
		this.zip_code = zip_code;
	}

	public String getPlace()
	{
		return place;
	}

	public void setPlace(String place)
	{
		this.place = place;
	}
}
