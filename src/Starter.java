import Identity.IdentityManager;
import Person.Person;

public class Starter
{
	public static void main(String[] args)
	{
		Person p1 = new Person("horst", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666",
				"h�lle");
		Person p2 = new Person("bernd", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666",
				"h�lle");
		Person p3 = new Person("ernie", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666",
				"h�lle");
		Person p4 = new Person("ernie", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666",
				"h�lle");
		Person p5 = new Person("horst", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666",
				"h�lle");
		Person p6 = new Person("bernd", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666",
				"h�lle");
		Person p7 = new Person("ernie", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666",
				"h�lle");
		Person p8 = new Person("ernie", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666",
				"h�lle");

		p1.delete();
		p3.delete();

		p3 = new Person("horst", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666", "h�lle");

		p1 = new Person("horst", "peter", "h.p@web.de", "123151", "superstrasse 9999", "12312312", "666", "h�lle");

		IdentityManager	.getSpecificIdentities(p1)
						.stream()
						.forEach(System.out::println);
	}
}