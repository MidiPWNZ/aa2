package Identity;

import java.lang.reflect.Field;

/**
 * Representation of an Identity.
 * 
 * Gets registered in the static <i>IdentityManager</i> identity space and
 * therefore receives an id.
 * 
 * @author aschulz
 *
 */
public abstract class Identity
{
	private final int id;

	public Identity()
	{
		id = IdentityManager.registerIdentity(this);
	}

	public int getId()
	{
		return id;
	}

	/**
	 * deletes the identity.
	 */
	public void delete()
	{
		IdentityManager.deleteIdentity(this);
		try {
			finalize();
		} catch (Throwable e) {
			System.err.println(this.getId() + " was not deletable!");
			e.printStackTrace();
		}
		System.gc();
	}

	public long getCount()
	{
		return IdentityManager.getIdentityStorage().get(this.getClass().getName()).size();
	}

	@Override
	public String toString()
	{
		String string = "id#" + this.id;
		for (Field field : this.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				string += ", " + String.join(": ", field.getName(), field.get(this).toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			field.setAccessible(false);
		}
		return string + ".";
	}
}
