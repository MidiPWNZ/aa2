package Identity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class IdentityManager
{

	private static IdentityManager instance;

	private static Map<String, ArrayList<Identity>> identityStorage;

	private static int[] idSpace;

	private IdentityManager()
	{
		identityStorage = new HashMap<String, ArrayList<Identity>>();
		idSpace = new int[1000];

		for (int i = 0; i < idSpace.length; i++) {
			idSpace[i] = idSpace.length + 1;
		}
	}

	/**
	 * Adds the given identity to the registry storage, with the given
	 * identity's reflected class name.
	 * 
	 * @param identity
	 * @return <b>int</b> id
	 */
	public static int registerIdentity(Identity identity)
	{
		if (IdentityManager.instance == null) {
			IdentityManager.instance = new IdentityManager();
		}

		int id = idSpace.length + 1;
		for (int i = 0; i < idSpace.length; i++) {
			if (id == idSpace.length + 1 && idSpace[i] == idSpace.length + 1) {
				idSpace[i] = i;
				id = idSpace[i];
			}
		}

		if (identityStorage.get(identity.getClass().getName()) == null) {
			identityStorage.put(identity.getClass().getName(), new ArrayList<Identity>());
		}
		identityStorage.get(identity.getClass().getName()).add(identity);

		sortSpecificIdentities(identity);
		return id;
	}

	/**
	 * Returns the full identity storage.
	 * 
	 * @return
	 */
	public static Map<String, ArrayList<Identity>> getIdentityStorage()
	{
		return identityStorage;
	}

	/**
	 * Returns the registry space, for given identity's reflected class name.
	 * 
	 * @param identity
	 * @return
	 */
	public static ArrayList<Identity> getSpecificIdentities(Identity identity)
	{
		return identityStorage.get(identity.getClass().getName());
	}

	/**
	 * Returns the count of identities in the registry storage, for given
	 * identity's reflected class name.
	 * 
	 * @param identity
	 * @return
	 */
	public static long getSpecificIdentityCount(Identity identity)
	{
		return identityStorage.get(identity.getClass().getName()).size();
	}

	/**
	 * Removes given identity's id from the id space and itself from the
	 * registry storage.
	 * 
	 * @param identity
	 */
	public static void deleteIdentity(Identity identity)
	{
		idSpace[identity.getId()] = idSpace.length + 1;

		identityStorage.get(identity.getClass().getName()).remove(identity);

		sortSpecificIdentities(identity);
	}

	private static void sortSpecificIdentities(Identity identity)
	{
		identityStorage	.get(identity.getClass().getName())
					.stream()
					.sorted((i1, i2) -> Integer.compare(i1.getId(), i2.getId()));
	}
}
